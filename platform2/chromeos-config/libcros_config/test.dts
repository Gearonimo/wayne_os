/*
 * Copyright 2016 The Chromium OS Authors. All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

/dts-v1/;

/ {
	chromeos {
		family: family {
		};
		models: models {
		};
	};
};

&family {
	audio {
		audio_type: audio-type {
			card = "a-card";
			volume = "cras-config/{cras-config-dir}/{card}";
			dsp-ini = "cras-config/{cras-config-dir}/dsp.ini";
			hifi-conf = "ucm-config/{card}.{ucm-suffix}/HiFi.conf";
			alsa-conf = "ucm-config/{card}.{ucm-suffix}/{card}.{ucm-suffix}.conf";
			topology-bin = "topology/{topology-name}-tplg.bin";
		};
	};
	firmware {
		shared: some {
			bcs-overlay = "overlay-some-private";
			ec-image = "bcs://Some_EC.1111.11.1.tbz2";
			main-image = "bcs://Some.1111.11.1.tbz2";
			main-rw-image = "bcs://Some_RW.1111.11.1.tbz2";
			build-targets {
				coreboot = "some";
				ec = "some";
				depthcharge = "some";
				libpayload = "some";
			};
		};
	};
	mapping {
		sku-map@0 {
			platform-name = "Some";
			smbios-name-match = "Some";
			/*
			 * This is an example! It does not match any real
			 * family.
			 */
			simple-sku-map = <
				0 &some_touch
				1 &some_notouch
				8 &some_whitelabel_touch
				9 &some_whitelabel_notouch>;
		};
		sku-map@1 {
			platform-name = "Another";
			smbios-name-match = "Another";
			single-sku = <&another>;
		};
	};
	touch {
		/* Example of how to put firmware in BCS */
		some_touchscreen: some-touchscreen {
			vendor = "some_touch_vendor";
			firmware-bin = "{vendor}/{pid}_{version}.bin";
			firmware-symlink = "{vendor}ts_i2c_{pid}.bin";
		};
		some_stylus: some-stylus {
			vendor = "some_stylus_vendor";
			firmware-bin = "{vendor}/{version}.hex";
			firmware-symlink = "{vendor}_firmware_{MODEL}.bin";
		};
	};
};

&models {
	some {
		wallpaper = "some";
		oem-id = "0";
		arc {
			hw-features = "some/hardware_features";
		};
		firmware {
			key-id = "SOME";
			shares = <&shared>;
		};
		modem {
			firmware-variant = "some";
		};
		submodels {
			some_touch: touch {
				touch {
					present = "yes";
				};
				audio {
					main {
						audio-type = <&audio_type>;
						cras-config-dir = "some";
						ucm-suffix = "some";
						topology-name = "some";
					};
				};
				thermal {
					dptf-dv = "some_touch/dptf.dv";
				};
			};
			some_notouch: notouch {
				touch {
					present = "no";
				};
				audio {
					main {
						audio-type = <&audio_type>;
						cras-config-dir = "some";
						ucm-suffix = "some";
						topology-name = "some";
					};
				};
				thermal {
					dptf-dv = "some_notouch/dptf.dv";
				};
			};
		};
		touch {
			stylus {
				touch-type = <&some_stylus>;
				version = "some-version";
			};
			touchpad {
				touch-type = <&some_touchscreen>;
				pid = "some-pid";
				version = "some-version";
			};
			touchscreen@0 {
				touch-type = <&some_touchscreen>;
				pid = "some-other-pid";
				version = "some-other-version";
			};
		};
		ui {
			power-button {
				edge = "left";
				position = "0.3";
			};
		};
	};
	another: another {
		string-list = "default", "more";
		bool-prop;
		wallpaper = "default";
		audio {
			main {
				audio-type = <&audio_type>;
				cras-config-dir = "another";
				ucm-suffix = "another";
				topology-name = "another";
			};
		};
		firmware {
			bcs-overlay = "overlay-another-private";
			ec-image = "bcs://Another_EC.1111.11.1.tbz2";
			main-image = "bcs://Another.1111.11.1.tbz2";
			main-rw-image = "bcs://Another_RW.1111.11.1.tbz2";
			key-id = "ANOTHER";
			extra = "${FILESDIR}/extra";
			tools = "${FILESDIR}/tools1", "${FILESDIR}/tools2";
			build-targets {
				base = "another_base";
				ec_extras = "extra1", "extra2";
				coreboot = "another";
				cr50 = "another_cr50";
				ec = "another";
				depthcharge = "another";
				libpayload = "another";
			};
		};
		thermal {
			dptf-dv = "another/dptf.dv";
		};
		touch {
			present = "probe";
			probe-regex = "another-probe-regex";
			touchscreen {
				touch-type = <&some_touchscreen>;
				pid = "some-pid";
				version = "some-version";
			};
			stylus {
				touch-type = <&some_stylus>;
				version = "another-version";
			};
		};
	};

	whitelabel: whitelabel {
		oem-id = "1";
		firmware {
			shares = <&shared>;
		};
		submodels {
			some_whitelabel_touch: touch {
				touch {
					present = "yes";
				};
			};
			some_whitelabel_notouch: notouch {
				touch {
					present = "no";
				};
			};
		};
		whitelabels {
			whitelabel1 {
				wallpaper = "wallpaper-wl1";
				brand-code = "WLBA";
				key-id = "WHITELABEL1";
			};
			whitelabel2 {
				wallpaper = "wallpaper-wl2";
				brand-code = "WLBB";
				key-id = "WHITELABEL2";
			};
		};
	};
};

/include/ "target_dirs.dtsi"
