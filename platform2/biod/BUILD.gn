# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/pkg_config.gni")

group("all") {
  deps = [
    ":bio_crypto_init",
    ":bio_fw_updater",
    ":bio_wash",
    ":biod",
    ":biod_client_tool",
    ":libbiod",
  ]
  if (use.test) {
    deps += [ ":biod_test_runner" ]
  }
  if (use.fuzzer) {
    deps += [
      ":biod_crypto_validation_value_fuzzer",
      ":biod_storage_fuzzer",
    ]
  }
}

pkg_config("target_defaults") {
  # TODO(crbug.com/993557): Remove |fp_on_power_button| USE flag when nocturne
  # is EOL.
  if (use.fp_on_power_button) {
    defines = [ "FP_ON_POWER_BUTTON" ]
  }
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "libcros_config",
    "libmetrics-${libbase_ver}",
    "libpower_manager-client",
    "fmap",
    "openssl",

    # system_api depends on protobuf (or protobuf-lite). It must appear
    # before protobuf here or the linker flags won't be in the right
    # order.
    "system_api",
    "protobuf-lite",
  ]
}

static_library("libbiod") {
  configs += [ ":target_defaults" ]
  sources = [
    "biod_crypto.cc",
    "biod_metrics.cc",
    "biod_storage.cc",
    "biometrics_daemon.cc",
    "cros_fp_biometrics_manager.cc",
    "cros_fp_device.cc",
    "cros_fp_firmware.cc",
    "cros_fp_updater.cc",
    "ec_command_factory.cc",
    "fp_context_command.cc",
    "fp_context_command_factory.cc",
    "fp_mode.cc",
    "power_button_filter.cc",
    "power_manager_client.cc",
    "scoped_umask.cc",
    "uinput_device.cc",
  ]
}

static_library("fake_libbiod") {
  configs += [ ":target_defaults" ]
  sources = [
    "fake_power_manager_client.cc",
  ]
}

executable("biod") {
  configs += [ ":target_defaults" ]
  sources = [
    "main.cc",
  ]
  deps = [
    ":libbiod",
  ]
}

executable("bio_crypto_init") {
  configs += [ ":target_defaults" ]
  sources = [
    "tools/bio_crypto_init.cc",
  ]
  deps = [
    ":libbiod",
  ]
}

executable("bio_wash") {
  configs += [ ":target_defaults" ]
  sources = [
    "tools/bio_wash.cc",
  ]
  deps = [
    ":libbiod",
  ]
}

executable("biod_client_tool") {
  configs += [ ":target_defaults" ]
  sources = [
    "tools/biod_client_tool.cc",
  ]
}

executable("bio_fw_updater") {
  configs += [ ":target_defaults" ]
  sources = [
    "tools/bio_fw_updater.cc",
  ]
  deps = [
    ":libbiod",
  ]
}

if (use.test) {
  pkg_config("libchrome_test_config") {
    pkg_deps = [ "libchrome-test-${libbase_ver}" ]
  }
  executable("biod_test_runner") {
    configs += [
      "//common-mk:test",
      ":libchrome_test_config",
      ":target_defaults",
    ]
    sources = [
      "biod_crypto_test.cc",
      "biod_metrics_test.cc",
      "biod_storage_test.cc",
      "cros_fp_biometrics_manager_test.cc",
      "cros_fp_device_test.cc",
      "cros_fp_firmware_test.cc",
      "cros_fp_updater_test.cc",
      "ec_command_async_test.cc",
      "ec_command_test.cc",
      "fp_context_command_factory_test.cc",
      "fp_context_command_test.cc",
      "fp_mode_test.cc",
      "power_button_filter_test.cc",
      "power_manager_client_test.cc",
      "tools/bio_crypto_init_test.cc",
    ]
    deps = [
      ":fake_libbiod",
      ":libbiod",
      "//common-mk/testrunner",
    ]
  }
}

if (use.fuzzer) {
  pkg_config("libchrome_test_config") {
    pkg_deps = [ "libchrome-test-${libbase_ver}" ]
  }
  executable("biod_storage_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":libchrome_test_config",
      ":target_defaults",
    ]
    sources = [
      "biod_storage_fuzzer.cc",
    ]
    deps = [
      ":libbiod",
    ]
  }
  executable("biod_crypto_validation_value_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":libchrome_test_config",
      ":target_defaults",
    ]
    sources = [
      "biod_crypto_validation_value_fuzzer.cc",
    ]
    deps = [
      ":libbiod",
    ]
  }
}
