// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// API exposed by the cros_healthd daemon. This API is normally consumed by the
// browser and the telem and diag command-line tools.

// NOTE: This mojom should be kept in sync with the copy in Chromium's repo in
// src/chrome/services/cros_healthd/public/mojom/cros_healthd.mojom.

module chromeos.cros_healthd.mojom;

import "cros_healthd_probe.mojom";

// Interface exposed by the cros_healthd daemon.
interface CrosHealthdService {
  // ------------------- Start of probe definitions ---------------------

  // Returns telemetry information for the desired categories.
  //
  // The request:
  // * |categories| - list of each of the categories that ProbeTelemetryInfo
  //                  should return information for.
  //
  // The response:
  // * |telemetry_info| - information for each of the requested categories. Only
  //                      the fields corresponding to the requested categories
  //                      will be non-null.
  ProbeTelemetryInfo(array<ProbeCategoryEnum> categories)
      => (TelemetryInfo telemetry_info);

  // ------------------- End of probe definitions -----------------------
};
