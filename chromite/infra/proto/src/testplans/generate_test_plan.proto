// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package testplans;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/testplans";

import "chromiumos/common.proto";
import "google/protobuf/struct.proto";
import "testplans/common.proto";
import "testplans/target_test_requirements_config.proto";

message GenerateTestPlanRequest {
  // Serialized buildbucket Build protos that are part of this orchestrator run.
  // See https://chromium.googlesource.com/infra/luci/luci-go/+/master/buildbucket/proto/build.proto
  repeated ProtoBytes buildbucket_protos = 5;

  // The manifest-internal snapshot commit hash that's being used for the current build.
  string manifest_commit = 7;
}

// The final test plan.
message GenerateTestPlanResponse {
  repeated GceTestUnit gce_test_units = 2;
  repeated HwTestUnit hw_test_units = 3;
  repeated MoblabVmTestUnit moblab_vm_test_units = 4;
  repeated TastVmTestUnit tast_vm_test_units = 5;
  repeated VmTestUnit vm_test_units = 6;
}

// The files that should be tested in a test plan.
message BuildPayload {
  // The GS bucket in which artifacts for the build are stored, e.g.
  // gs://chromeos-image-archive
  string artifacts_gs_bucket = 1;

  // The path in the bucket in which artifacts for the build are stored, e.g.
  // eve-paladin/R73-11588.0.0-rc4
  string artifacts_gs_path = 2;

  // Artifact type to files available for that type. e.g.
  // "AUTOTEST_FILES": [ "control_files.tar" ]
  google.protobuf.Struct files_by_artifact = 3;
}

// The parts of a TestUnit that are common among all test types.
message TestUnitCommon {
  // The build target for this test unit.
  chromiumos.BuildTarget build_target = 2;

  // The build files provided to run this test unit.
  BuildPayload build_payload = 3;
}

message GceTestUnit {
  TestUnitCommon common = 1;
  GceTestCfg gce_test_cfg = 2;
}

message HwTestUnit {
  TestUnitCommon common = 1;
  HwTestCfg hw_test_cfg = 2;
}

message MoblabVmTestUnit {
  TestUnitCommon common = 1;
  MoblabVmTestCfg moblab_vm_test_cfg = 2;
}

message TastVmTestUnit {
  TestUnitCommon common = 1;
  TastVmTestCfg tast_vm_test_cfg = 2;
}

message VmTestUnit {
  TestUnitCommon common = 1;
  VmTestCfg vm_test_cfg = 2;
}

