# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Configure the database for moblab"
author        "chromium-os-dev@chromium.org"

start on stopped moblab-autotest-setup
respawn

normal exit 0

env EXTERNAL_RESULTS_DIR=/mnt/moblab/results

script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  logger -t "${UPSTART_JOB}" "Pre Start Starting."
  MYSQL_LIB_DIR=/var/lib/mysql
  if [ ! -d "${MYSQL_LIB_DIR}" ]; then
    mkdir -p "${MYSQL_LIB_DIR}"
    chown -R mysql:mysql "${MYSQL_LIB_DIR}"
  fi

  MYSQL_RUN_DIR=/run/mysqld
  if [ ! -d "${MYSQL_RUN_DIR}" ]; then
    mkdir -p "${MYSQL_RUN_DIR}"
    chown -R mysql:mysql "${MYSQL_RUN_DIR}"
  fi

  MYSQL_LOG_DIR=/var/log/mysql
  if [ ! -d "${MYSQL_LOG_DIR}" ]; then
    mkdir -p "${MYSQL_LOG_DIR}"
    chown -R mysql:mysql "${MYSQL_LOG_DIR}"
  fi

  # Clean up the binary logs if they still exist.
  if [ -e "${MYSQL_LIB_DIR}/mysqld-bin.000001" ]; then
    rm "${MYSQL_LIB_DIR}"/mysqld-bin.*
  fi

  # The below command won't work if the database has already been installed
  # as it is not supplying the set password.
  /usr/share/mariadb/scripts/mysql_install_db --user=mysql \
    --ldata="${MYSQL_LIB_DIR}" --basedir=/usr/ --force \
    --defaults-extra-file=/etc/moblab/mysql/mysql_defaults_extra.cnf \
    --skip-name-resolve || :
  exec /usr/sbin/mysqld \
    --defaults-extra-file=/etc/moblab/mysql/mysql_defaults_extra.cnf
end script

post-start script
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  logger -t "$UPSTART_JOB" "Starting."
  # Wait for the service to come up.
  SLEEP_TIME=2
  while [ ! -e /run/mysqld/mysqld.sock ]; do
    logger -t "$UPSTART_JOB" "mysqld not up. Sleeping ${SLEEP_TIME} seconds."
    sleep ${SLEEP_TIME}
  done
  passwd=$(sed -n '/^password: /{s,^[^:]*: *,,p;q}' \
    /usr/local/autotest/shadow_config.ini)
  if [ -z "$passwd" ]; then
    passwd=$(sed -n '/^password = /{s,^[^=]*= *,,p;q}' \
      /usr/local/autotest/shadow_config.ini)
  fi
  if mysql -u root; then
    mysqladmin -u root password "${passwd}"
    # In the case we have updated to a new version of the db - upgrade
    mysqlcheck --no-defaults --fix-db-names --fix-table-names -uroot \
      -p"${passwd}" --all-databases -v
    mysqlcheck --no-defaults --check-upgrade --auto-repair -uroot \
      -p"${passwd}" --all-databases -v
  fi

  PASSWD_STRING="-p${passwd}"

  # Create autotest database
  logger -t "$UPSTART_JOB" "Create database."
  clobberdb=
  DATABASE_LOOKUP_SQL="SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA \
    WHERE SCHEMA_NAME = 'chromeos_autotest_db'"
  EXISTING_DATABASE=$(mysql -u root "${PASSWD_STRING}" -e "${DATABASE_LOOKUP_SQL}")
  if [ -z "${EXISTING_DATABASE}" ]; then
    clobberdb=true
  fi

  SQL_COMMAND="drop database if exists chromeos_autotest_db; \
    create database chromeos_autotest_db; \
    grant all privileges on chromeos_autotest_db.* TO \
    'chromeosqa-admin'@'localhost' identified by '${passwd}'; \
    FLUSH PRIVILEGES;"

  if [ "${clobberdb}" = true ]; then
    mysql -u root "${PASSWD_STRING}" -e "${SQL_COMMAND}"
  fi

  logger -t "$UPSTART_JOB" "Database Migration."
  AT_DIR="/usr/local/autotest"
  python2.7 "${AT_DIR}"/database/migrate.py sync -f
  python2.7 "${AT_DIR}"/database/migrate.py sync -f -d TKO
  python2.7 "${AT_DIR}"/frontend/manage.py syncdb --noinput
  # You may have to run this twice.
  python2.7 "${AT_DIR}"/frontend/manage.py syncdb --noinput

  # Create whining database
  logger -t "$UPSTART_JOB" "Create whining database."
  WHINING_DIR="/whining"
  cd "${WHINING_DIR}"/src/backend/sql

  DATABASE_LOOKUP_SQL="SELECT User FROM mysql.user WHERE User='wmreader';"
  EXISTING_WMREADER=$(mysql -u root "${PASSWD_STRING}" -e "${DATABASE_LOOKUP_SQL}")
  DATABASE_LOOKUP_SQL="SELECT User FROM mysql.user WHERE User='wmatrix';"
  EXISTING_WMATRIX=$(mysql -u root "${PASSWD_STRING}" -e "${DATABASE_LOOKUP_SQL}")
  if [ -z "${EXISTING_WMREADER}" -o -z "${EXISTING_WMATRIX}" ]; then
    sh createusers_moblab.sh "${passwd}" <<EOF
wmreader0000
wmatrix0000
EOF
  fi

  DATABASE_LOOKUP_SQL="SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA \
    WHERE SCHEMA_NAME = 'rawdb'"
  EXISTING_RAWDB=$(mysql -u root "${PASSWD_STRING}" -e "${DATABASE_LOOKUP_SQL}")
  if [ -z "${EXISTING_RAWDB}" ]; then
    sh rawrecreate_moblab.sh "${passwd}" rawdb
  fi

  DATABASE_LOOKUP_SQL="SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA \
    WHERE SCHEMA_NAME = 'wmdb'"
  EXISTING_WMDB=$(mysql -u root "${PASSWD_STRING}" -e "${DATABASE_LOOKUP_SQL}")
  if [ -z "${EXISTING_WMDB}" ]; then
    sh recreate_moblab.sh "${passwd}" wmdb
  fi

  logger -t "$UPSTART_JOB" "Ending."

  # If the database is empty clear out any old files in the results directory.
  jobcount="$(mysql -u root -pmoblab_db_passwd -D chromeos_autotest_db \
              -e 'SELECT COUNT(*) FROM afe_jobs' | tail -1)"
  if [ "${jobcount}" = "0" ]; then
    rm -rf "${EXTERNAL_RESULTS_DIR}"/*
  fi

end script

