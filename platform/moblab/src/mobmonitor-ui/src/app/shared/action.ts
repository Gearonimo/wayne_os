export interface Action {
  action: string;
  service: string;
  healthCheck: string;
}
