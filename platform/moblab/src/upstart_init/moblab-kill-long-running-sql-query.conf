# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Service to kill long running queries on moblab"
author        "chromium-os-dev@chromium.org"

start on started moblab-database-init

respawn

script
  set -x -e
  while true; do
    passwd=$(sed -n '/^password: /{s,^[^:]*: *,,p;q}' \
      /usr/local/autotest/shadow_config.ini)
    if [ -z "$passwd" ]; then
      passwd=$(sed -n '/^password = /{s,^[^=]*= *,,p;q}' \
        /usr/local/autotest/shadow_config.ini)
    fi
    SECONDS_TOO_LONG=1200
    TOO_LONG_QUERY="SELECT COUNT(1) FROM information_schema.processlist\
                    WHERE user<>'system user' \
                    AND time >= ${SECONDS_TOO_LONG}"
    RUNNING_TOO_LONG=`mysql -uroot -p"${passwd}" -ANe "${TOO_LONG_QUERY}"`
    if [ ${RUNNING_TOO_LONG} -gt 0 ]
    then
        mkdir -p /var/log/bootup/
        exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
        logger -t "${UPSTART_JOB}" "Killing ${RUNNING_TOO_LONG} processes."
        KILLPROC_SQLSTMT="SELECT GROUP_CONCAT(CONCAT('KILL QUERY ',id,';')
                                              SEPARATOR ' ') KillQuery \
                          FROM information_schema.processlist \
                          WHERE user<>'system user' \
                          AND time >= ${SECONDS_TOO_LONG}"
        mysql -uroot -p"${passwd}" -ANe "${KILLPROC_SQLSTMT}" |
            mysql -uroot -p"${passwd}"
    fi
    sleep 1200
  done

end script
