import {MapToIterable} from './map-to-iterable';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [
    MapToIterable,
  ],
  exports: [
    MapToIterable,
  ],
  imports: [
  ]
})

export class UtilsModule { }
