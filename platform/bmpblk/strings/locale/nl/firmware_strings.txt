Chrome OS ontbreekt of is beschadigd.
Plaats een herstel-USB-stick of herstel-SD-kaart.
Plaats een herstel-USB-stick.
Plaats een herstel-SD-kaart of herstel-USB-stick. (Opmerking: De blauwe USB-poort werkt NIET voor herstel.)
Plaats een herstel-USB-stick in een van de vier poorten aan de ACHTERKANT van het apparaat.
Chrome OS staat niet op het apparaat dat u heeft geplaatst.
OS-verificatie staat UIT
Druk op de SPATIEBALK om dit opnieuw in te schakelen.
Druk op ENTER om te bevestigen dat u OS-verificatie wilt inschakelen.
Uw systeem wordt opnieuw opgestart en de lokale gegevens worden gewist.
Druk op ESC om terug te gaan.
OS-verificatie staat AAN.
Druk op ENTER om OS-verificatie uit te schakelen.
Ga voor ondersteuning naar https://google.com/chromeos/recovery
Foutcode
Verwijder alle externe apparaten om het herstelproces te starten.
Model 60061e
Als u OS-verificatie UIT wilt zetten, drukt u op de HERSTELKNOP.
De aangesloten voeding levert niet voldoende stroom voor dit apparaat.
Chrome OS wordt nu uitgeschakeld.
Gebruik de juiste adapter en probeer het opnieuw.
Verwijder alle gekoppelde apparaten en start het herstelproces.
Druk op een cijfertoets om een alternatieve bootloader te kiezen:
Druk op de aan/uit-knop om een diagnose uit te voeren.
Als je de OS-verificatie UIT wilt zetten, druk je op de AAN/UIT-KNOP.
