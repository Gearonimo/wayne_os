O Chrome OS está ausente ou danificado.
Insira um cartão SD ou USB de recuperação.
Insira um cartão USB de recuperação.
Insira um cartão SD ou USB de recuperação (observação: a porta USB azul NÃO funciona para recuperação).
Insira um cartão USB de recuperação em uma das quatro portas na PARTE TRASEIRA do dispositivo.
O dispositivo que você inseriu não contém o Chrome OS.
O recurso Verificação do SO está DESATIVADO
Pressione a tecla ESPAÇO para reativar.
Pressione a tecla ENTER para confirmar que você quer ativar o recurso Verificação do SO.
Seu sistema será reiniciado, e os dados locais serão removidos.
Para voltar, pressione a tecla ESC.
O recurso Verificação do SO está ATIVADO.
Para DESATIVAR o recurso Verificação do SO, pressione ENTER.
Para receber ajuda, acesse https://google.com/chromeos/recovery
Código do erro
Remova todos os dispositivos externos para começar a recuperação.
Modelo 60061e
Para desativar o recurso Verificações do SO, pressione o botão RECUPERAÇÃO.
A fonte de alimentação conectada não tem energia suficiente para executar este dispositivo.
O Chrome OS será desligado agora.
Use o adaptador correto e tente novamente.
Remova todos os dispositivos conectados e inicie a recuperação.
Pressione uma tecla numérica para selecionar um carregador de inicialização alternativo:
Pressione o botão liga/desliga para executar o diagnóstico.
Para DESATIVAR a verificação do SO, pressione o botão LIGA/DESLIGA.
