#!/bin/sh
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

PROG=$(basename "$0")
TTY=/dev/tty1
BATTERY_FIRMWARE_UPDATE_LOGS=/mnt/stateful_partition/battery_update_firmware.log

log_msg() {
  logger -t "${PROG}[$$]" "$@"
  echo "${PROG}[$$]" "$@" | tee -a "${BATTERY_FIRMWARE_UPDATE_LOGS}"
}

# Check if the battery has the lastest firmware or not.
# Disable the battery interface if the battery got the latest firmware.
ec_sb_firmware_update check > "${BATTERY_FIRMWARE_UPDATE_LOGS}"
case "$?" in
  40)
    log_msg 'Type "battery_firmware update" in crosh.'
    ;;
  8)
    log_msg 'Type "battery_firmware update" in crosh.'
    ;;
  0)
    log_msg 'Got the latest battery firmware.'
    ;;
  *)
    log_msg "Check ${BATTERY_FIRMWARE_UPDATE_LOGS}."
    ;;
esac
