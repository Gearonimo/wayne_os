This directory contains the example of manifest files.

manifest.json is bundled in the Huddly firmware package file.
manifest.txt is derived from manifest.json, and contains essential
information only. The derivation is done by huddly-firmware ebuild.
