Qualcomm WCSS Clock Controller Binding
-----------------------------------------------

Required properties :
- compatible		: shall contain "qcom,qcs404-wcsscc"
- #clock-cells		: from common clock binding, shall contain 1.
- reg			: shall contain base register address and size,
			  in the order
			Index-0 maps to WCSS_Q6SSTOP clocks register region
			Index-1 maps to WCSS_TCSR register region
			Index-2 maps to WCSS_QDSP6SS register region

Optional properties :
- reg-names	: register names of WCSS domain
		 "wcss_q6sstop", "wcnss_tcsr", "wcss_qdsp6ss".

Example:
The below node has to be defined in the cases where the WCSS peripheral loader
would bring the subsystem out of reset.

	clock_wcsscc: qcom,wcsscc@7000000 {
		compatible = "qcom,qcs404-wcsscc";
		reg = <0x07500000 0x4e000>, <0x07550000 0x8012>, <0x07400000 0x104>;
		reg-names = "wcss_q6sstop", "wcnss_tcsr", "wcss_qdsp6ss";
		#clock-cells = <1>;
	};
