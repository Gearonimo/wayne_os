From 8c7e9c7825d6c63144217abc1d13ee4ea3e7fd4a Mon Sep 17 00:00:00 2001
From: Hidehiko Abe <hidehiko@chromium.org>
Date: Thu, 13 Sep 2018 16:49:34 +0900
Subject: [PATCH] libmojo: Prepare r456626 uprev.

This CL adds forward compatibility;
- New InitIPCSupport and ShutdownIPCSupport. Those signature are
  changed on r456626 uprev.
- StrongBinding::Create, MakeStrongBinding is added.
- Introduce alias MakeRequest for GetProxy, and ConnectToPeerProcess for
  CreateMessagePipe.

BUG=b:37434548
TEST=Ran trybot.
---
 mojo/edk/embedder/embedder.cc                | 28 ++++++++++++++++++--
 mojo/edk/embedder/embedder.h                 |  8 ++++++
 mojo/public/cpp/bindings/interface_request.h |  8 ++++++
 mojo/public/cpp/bindings/strong_binding.h    | 13 +++++++++
 4 files changed, 55 insertions(+), 2 deletions(-)

diff --git a/mojo/edk/embedder/embedder.cc b/mojo/edk/embedder/embedder.cc
index 38c789c..d86bf6a 100644
--- a/mojo/edk/embedder/embedder.cc
+++ b/mojo/edk/embedder/embedder.cc
@@ -34,6 +34,22 @@ Core* GetCore() { return g_core; }
 
 }  // namespace internal
 
+namespace {
+class StubDelegate : public ProcessDelegate {
+ public:
+  void OnShutdownComplete() override {
+    delete this;
+  }
+};
+
+void OnShutdownComplete(const base::Closure& callback,
+                        ProcessDelegate* delegate) {
+  if (!callback.is_null())
+    callback.Run();
+  delegate->OnShutdownComplete();
+}
+}  // namespace
+
 void SetMaxMessageSize(size_t bytes) {
 }
 
@@ -104,6 +120,11 @@ MojoResult PassSharedMemoryHandle(
       mojo_handle, shared_memory_handle, num_bytes, read_only);
 }
 
+void InitIPCSupport(scoped_refptr<base::TaskRunner> io_thread_task_runner) {
+  // Note that StubDelegate will be deleted in OnShutdownComplete().
+  InitIPCSupport(new StubDelegate(), io_thread_task_runner);
+}
+
 void InitIPCSupport(ProcessDelegate* process_delegate,
                     scoped_refptr<base::TaskRunner> io_thread_task_runner) {
   CHECK(internal::g_core);
@@ -112,11 +133,14 @@ void InitIPCSupport(ProcessDelegate* process_delegate,
 }
 
 void ShutdownIPCSupport() {
+  ShutdownIPCSupport(base::Closure());
+}
+
+void ShutdownIPCSupport(const base::Closure& callback) {
   CHECK(internal::g_process_delegate);
   CHECK(internal::g_core);
   internal::g_core->RequestShutdown(
-      base::Bind(&ProcessDelegate::OnShutdownComplete,
-                 base::Unretained(internal::g_process_delegate)));
+      base::Bind(&OnShutdownComplete, callback, internal::g_process_delegate));
 }
 
 #if defined(OS_MACOSX) && !defined(OS_IOS)
diff --git a/mojo/edk/embedder/embedder.h b/mojo/edk/embedder/embedder.h
index edb6c32..307d3bf 100644
--- a/mojo/edk/embedder/embedder.h
+++ b/mojo/edk/embedder/embedder.h
@@ -131,12 +131,15 @@ PassSharedMemoryHandle(MojoHandle mojo_handle,
 MOJO_SYSTEM_IMPL_EXPORT void InitIPCSupport(
     ProcessDelegate* process_delegate,
     scoped_refptr<base::TaskRunner> io_thread_task_runner);
+MOJO_SYSTEM_IMPL_EXPORT void InitIPCSupport(
+    scoped_refptr<base::TaskRunner> io_thread_task_runner);
 
 // Shuts down the subsystem initialized by |InitIPCSupport()|. It be called from
 // any thread and will attempt to complete shutdown on the I/O thread with which
 // the system was initialized. Upon completion the ProcessDelegate's
 // |OnShutdownComplete()| method is invoked.
 MOJO_SYSTEM_IMPL_EXPORT void ShutdownIPCSupport();
+MOJO_SYSTEM_IMPL_EXPORT void ShutdownIPCSupport(const base::Closure& callback);
 
 #if defined(OS_MACOSX) && !defined(OS_IOS)
 // Set the |base::PortProvider| for this process. Can be called on any thread,
@@ -154,6 +157,11 @@ MOJO_SYSTEM_IMPL_EXPORT void SetMachPortProvider(
 MOJO_SYSTEM_IMPL_EXPORT ScopedMessagePipeHandle
 CreateMessagePipe(ScopedPlatformHandle platform_handle);
 
+MOJO_SYSTEM_IMPL_EXPORT inline ScopedMessagePipeHandle
+ConnectToPeerProcess(ScopedPlatformHandle platform_handle) {
+  return CreateMessagePipe(std::move(platform_handle));
+}
+
 // Creates a message pipe from a token. A child embedder must also have this
 // token and call CreateChildMessagePipe() with it in order for the pipe to get
 // connected.
diff --git a/mojo/public/cpp/bindings/interface_request.h b/mojo/public/cpp/bindings/interface_request.h
index 3d6b27c..93d4e6e 100644
--- a/mojo/public/cpp/bindings/interface_request.h
+++ b/mojo/public/cpp/bindings/interface_request.h
@@ -123,6 +123,14 @@ InterfaceRequest<Interface> GetProxy(
   return MakeRequest<Interface>(std::move(pipe.handle1));
 }
 
+template <typename Interface>
+InterfaceRequest<Interface> MakeRequest(
+    InterfacePtr<Interface>* ptr,
+    scoped_refptr<base::SingleThreadTaskRunner> runner =
+        base::ThreadTaskRunnerHandle::Get()) {
+  return GetProxy(ptr, runner);
+}
+
 // Fuses an InterfaceRequest<T> endpoint with an InterfacePtrInfo<T> endpoint.
 // Returns |true| on success or |false| on failure.
 template <typename Interface>
diff --git a/mojo/public/cpp/bindings/strong_binding.h b/mojo/public/cpp/bindings/strong_binding.h
index 3694c68..e504f79 100644
--- a/mojo/public/cpp/bindings/strong_binding.h
+++ b/mojo/public/cpp/bindings/strong_binding.h
@@ -51,6 +51,12 @@ class StrongBinding {
   MOVE_ONLY_TYPE_FOR_CPP_03(StrongBinding);
 
  public:
+  static void Create(std::unique_ptr<Interface> impl,
+                     InterfaceRequest<Interface> request) {
+    // In OnConnectionError, impl is deleted.
+    new StrongBinding<Interface>(impl.release(), std::move(request));
+  }
+
   explicit StrongBinding(Interface* impl) : binding_(impl) {}
 
   StrongBinding(Interface* impl, ScopedMessagePipeHandle handle)
@@ -115,6 +121,13 @@ class StrongBinding {
   Binding<Interface> binding_;
 };
 
+template <typename Interface, typename Impl>
+void MakeStrongBinding(
+    std::unique_ptr<Impl> impl,
+    InterfaceRequest<Interface> request) {
+  StrongBinding<Interface>::Create(std::move(impl), std::move(request));
+}
+
 }  // namespace mojo
 
 #endif  // MOJO_PUBLIC_CPP_BINDINGS_STRONG_BINDING_H_
-- 
2.19.0.rc2.392.g5ba43deb5a-goog

