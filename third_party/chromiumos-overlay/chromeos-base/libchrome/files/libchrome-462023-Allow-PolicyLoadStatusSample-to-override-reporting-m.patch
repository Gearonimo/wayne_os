From 6410def39290ed829e0d56b92ab03ddd985a0c04 Mon Sep 17 00:00:00 2001
From: ljusten <ljusten@chromium.org>
Date: Fri, 5 May 2017 08:20:44 -0700
Subject: [PATCH] Allow PolicyLoadStatusSample to override reporting method

PolicyLoadStatusSample reports stats to base::LinearHistogram by
default. However, when used from libchrome in Chrome OS, this does
not work and causes memory leaks. This CL introduces PolicyLoadStatusSampler, which just gathers samples without doing anything with them, and its derived class PolicyLoadStatusAutoSubmitter, which sends them to base::LinearHistogram in its destructor and matches the old behavior. The slightly different name '*SampleR' should prevent accidental usage. Chrome OS can introduce its own submitter class or use the sampler class to dispose samples.

BUG=717888
TEST=Added test and ran it

Review-Url: https://codereview.chromium.org/2860973002
Cr-Commit-Position: refs/heads/master@{#469654}
---
 .../policy/core/common/policy_load_status.cc  | 29 ++++++++-------
 .../policy/core/common/policy_load_status.h   | 35 ++++++++++++-------
 2 files changed, 39 insertions(+), 25 deletions(-)

diff --git a/components/policy/core/common/policy_load_status.cc b/components/policy/core/common/policy_load_status.cc
index 71c5059a7fd3..1495d7c545ef 100644
--- a/components/policy/core/common/policy_load_status.cc
+++ b/components/policy/core/common/policy_load_status.cc
@@ -4,6 +4,7 @@
 
 #include "components/policy/core/common/policy_load_status.h"
 
+#include "base/bind.h"
 #include "base/metrics/histogram.h"
 #include "base/strings/stringprintf.h"
 #include "components/policy/core/common/policy_types.h"
@@ -16,23 +17,27 @@ const char kHistogramName[] = "Enterprise.PolicyLoadStatus";
 
 }  // namespace
 
-PolicyLoadStatusSample::PolicyLoadStatusSample()
-    : histogram_(base::LinearHistogram::FactoryGet(
-          kHistogramName, 1, POLICY_LOAD_STATUS_SIZE,
-          POLICY_LOAD_STATUS_SIZE + 1,
-          base::Histogram::kUmaTargetedHistogramFlag)) {
+PolicyLoadStatusSampler::PolicyLoadStatusSampler() {
   Add(POLICY_LOAD_STATUS_STARTED);
 }
 
-PolicyLoadStatusSample::~PolicyLoadStatusSample() {
-  for (int i = 0; i < POLICY_LOAD_STATUS_SIZE; ++i) {
-    if (status_bits_[i])
-      histogram_->Add(i);
-  }
-}
+PolicyLoadStatusSampler::~PolicyLoadStatusSampler() {}
 
-void PolicyLoadStatusSample::Add(PolicyLoadStatus status) {
+void PolicyLoadStatusSampler::Add(PolicyLoadStatus status) {
   status_bits_[status] = true;
 }
 
+PolicyLoadStatusUmaReporter::PolicyLoadStatusUmaReporter() {}
+
+PolicyLoadStatusUmaReporter::~PolicyLoadStatusUmaReporter() {
+  base::HistogramBase* histogram(base::LinearHistogram::FactoryGet(
+      kHistogramName, 1, POLICY_LOAD_STATUS_SIZE, POLICY_LOAD_STATUS_SIZE + 1,
+      base::Histogram::kUmaTargetedHistogramFlag));
+
+  for (int i = 0; i < POLICY_LOAD_STATUS_SIZE; ++i) {
+    if (GetStatusSet()[i])
+      histogram->Add(i);
+  }
+}
+
 }  // namespace policy
diff --git a/components/policy/core/common/policy_load_status.h b/components/policy/core/common/policy_load_status.h
index 6c9e907152f4..dc3979ef1e1c 100644
--- a/components/policy/core/common/policy_load_status.h
+++ b/components/policy/core/common/policy_load_status.h
@@ -10,10 +10,6 @@
 #include "base/macros.h"
 #include "components/policy/policy_export.h"
 
-namespace base {
-class HistogramBase;
-}
-
 namespace policy {
 
 // UMA histogram enum for policy load status. Don't change existing constants,
@@ -44,22 +40,35 @@ enum PolicyLoadStatus {
   POLICY_LOAD_STATUS_SIZE
 };
 
-// A helper for generating policy load status UMA statistics that'll collect
-// histogram samples for a policy load operation and records histogram samples
-// for the status codes that were seen on destruction.
-class POLICY_EXPORT PolicyLoadStatusSample {
+// A helper for collecting statuses for a policy load operation.
+class POLICY_EXPORT PolicyLoadStatusSampler {
  public:
-  PolicyLoadStatusSample();
-  ~PolicyLoadStatusSample();
+  using StatusSet = std::bitset<POLICY_LOAD_STATUS_SIZE>;
+
+  PolicyLoadStatusSampler();
+  virtual ~PolicyLoadStatusSampler();
 
   // Adds a status code.
   void Add(PolicyLoadStatus status);
 
+  // Returns a set with all statuses.
+  const StatusSet& GetStatusSet() const { return status_bits_; }
+
  private:
-  std::bitset<POLICY_LOAD_STATUS_SIZE> status_bits_;
-  base::HistogramBase* histogram_;
+  StatusSet status_bits_;
+  DISALLOW_COPY_AND_ASSIGN(PolicyLoadStatusSampler);
+};
 
-  DISALLOW_COPY_AND_ASSIGN(PolicyLoadStatusSample);
+// A helper for generating policy load status UMA statistics. On destruction,
+// records histogram samples for the collected status codes.
+class POLICY_EXPORT PolicyLoadStatusUmaReporter
+    : public PolicyLoadStatusSampler {
+ public:
+  PolicyLoadStatusUmaReporter();
+  ~PolicyLoadStatusUmaReporter() override;
+
+ private:
+  DISALLOW_COPY_AND_ASSIGN(PolicyLoadStatusUmaReporter);
 };
 
 }  // namespace policy
