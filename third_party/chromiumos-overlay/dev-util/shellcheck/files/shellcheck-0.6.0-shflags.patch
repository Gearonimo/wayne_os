From 936be36a5eb4c8db6bc128f7f07ef99f93821c5c Mon Sep 17 00:00:00 2001
From: Benjamin Gordon <bmgordon@chromium.org>
Date: Fri, 31 May 2019 10:42:53 -0600
Subject: [PATCH 1/2] Add support for basic shflags semantics

The shflags command-line flags library creates variables at runtime
with a few well-defined functions.  This causes shellcheck to spit out
lots of warnings about unassigned variables, as well as miss warnings
about unused flag variables.

We can address this with two parts:

1. Pretend that the shflags global variables are predefined like other
   shell variables so that shellcheck doesn't expect users to set them.
2. Treat DEFINE_string, DEFINE_int, etc. as new commands that create
   variables, similar to the existing read, local, mapfile, etc.

Part 1 can theoretically be addresssed without this by following sourced
files, but that doesn't help if people are otherwise not following
external sources.

The new behavior is on by default, similar to automatic bats test
behavior.

Addresses #1597

Avoid defining flags for non-literal parameters

Fix botched variable usage
---
 src/ShellCheck/Analytics.hs   |  3 +++
 src/ShellCheck/AnalyzerLib.hs | 11 +++++++++++
 src/ShellCheck/Data.hs        | 14 ++++++++++++--
 3 files changed, 26 insertions(+), 2 deletions(-)

diff --git a/src/ShellCheck/Analytics.hs b/src/ShellCheck/Analytics.hs
index 3002e3e..283213f 100644
--- a/src/ShellCheck/Analytics.hs
+++ b/src/ShellCheck/Analytics.hs
@@ -1835,6 +1835,9 @@ prop_checkUnused37= verifyNotTree checkUnusedAssignments "fd=2; exec {fd}>&-"
 prop_checkUnused38= verifyTree checkUnusedAssignments "(( a=42 ))"
 prop_checkUnused39= verifyNotTree checkUnusedAssignments "declare -x -f foo"
 prop_checkUnused40= verifyNotTree checkUnusedAssignments "arr=(1 2); num=2; echo \"${arr[@]:num}\""
+prop_checkUnused42= verifyNotTree checkUnusedAssignments "DEFINE_string foo '' ''; echo \"${FLAGS_foo}\""
+prop_checkUnused43= verifyTree checkUnusedAssignments "DEFINE_string foo '' ''"
+prop_checkUnused44= verifyNotTree checkUnusedAssignments "DEFINE_string \"foo$ibar\" x y"
 checkUnusedAssignments params t = execWriter (mapM_ warnFor unused)
   where
     flow = variableFlow params
diff --git a/src/ShellCheck/AnalyzerLib.hs b/src/ShellCheck/AnalyzerLib.hs
index de3498d..28398ee 100644
--- a/src/ShellCheck/AnalyzerLib.hs
+++ b/src/ShellCheck/AnalyzerLib.hs
@@ -569,6 +569,11 @@ getModifiedVariableCommand base@(T_SimpleCommand _ _ (T_NormalWord _ (T_Literal
         "mapfile" -> maybeToList $ getMapfileArray base rest
         "readarray" -> maybeToList $ getMapfileArray base rest
 
+        "DEFINE_boolean" -> maybeToList $ getFlagVariable rest
+        "DEFINE_float" -> maybeToList $ getFlagVariable rest
+        "DEFINE_integer" -> maybeToList $ getFlagVariable rest
+        "DEFINE_string" -> maybeToList $ getFlagVariable rest
+
         _ -> []
   where
     flags = map snd $ getAllFlags base
@@ -638,6 +643,12 @@ getModifiedVariableCommand base@(T_SimpleCommand _ _ (T_NormalWord _ (T_Literal
         map (getLiteralArray . snd)
             (filter (\(x,_) -> getLiteralString x == Just "-a") (zip (args) (tail args)))
 
+    -- get the FLAGS_ variable created by a shflags DEFINE_ call
+    getFlagVariable (n:v:_) = do
+        name <- getLiteralString n
+        return (base, n, "FLAGS_" ++ name, DataString $ SourceExternal)
+    getFlagVariable _ = Nothing
+
 getModifiedVariableCommand _ = []
 
 getIndexReferences s = fromMaybe [] $ do
diff --git a/src/ShellCheck/Data.hs b/src/ShellCheck/Data.hs
index 5b201c1..4497b6f 100644
--- a/src/ShellCheck/Data.hs
+++ b/src/ShellCheck/Data.hs
@@ -597,16 +597,26 @@ internalVariables = [
 
     -- python-utils-r1.eclass declared incorrectly
     "RESTRICT_PYTHON_ABIS", "PYTHON_MODNAME"
+
+    -- shflags
+    , "FLAGS_ARGC", "FLAGS_ARGV", "FLAGS_ERROR", "FLAGS_FALSE", "FLAGS_HELP",
+    "FLAGS_PARENT", "FLAGS_RESERVED", "FLAGS_TRUE", "FLAGS_VERSION",
+    "flags_error", "flags_return"
   ]
 
-variablesWithoutSpaces = [
-    "$", "-", "?", "!", "#",
+specialVariablesWithoutSpaces = [
+    "$", "-", "?", "!", "#"
+  ]
+variablesWithoutSpaces = specialVariablesWithoutSpaces ++ [
     "BASHPID", "BASH_ARGC", "BASH_LINENO", "BASH_SUBSHELL", "EUID", "LINENO",
     "OPTIND", "PPID", "RANDOM", "SECONDS", "SHELLOPTS", "SHLVL", "UID",
     "COLUMNS", "HISTFILESIZE", "HISTSIZE", "LINES",
 
     -- ebuild read-only variables
     "P", "PN", "PV", "PR", "PVR", "PF", "EAPI", "SLOT"
+
+    -- shflags
+    , "FLAGS_ERROR", "FLAGS_FALSE", "FLAGS_TRUE"
   ]
 
 arrayVariables = [
-- 
2.22.0.410.gd8fdbe21b5-goog

