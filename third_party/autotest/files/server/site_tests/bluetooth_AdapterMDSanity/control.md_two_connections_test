# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils
from autotest_lib.server.cros.bluetooth import bluetooth_test

AUTHOR = 'chromeos-bluetooth'
NAME = 'bluetooth_AdapterMDSanity.md_two_connections_test'
PURPOSE = ('Verify DUT can connect to both devices')
CRITERIA = 'DUT can connect to both devices'
ATTRIBUTES = 'suite:bluetooth_flaky, suite:bluetooth_e2e'
TIME = 'SHORT'
TEST_CATEGORY = 'Functional'
TEST_CLASS = 'bluetooth'
TEST_TYPE = 'server'
DEPENDENCIES = 'bluetooth, chameleon:bt_hid, chameleon:bt_ble_hid'

DOC = """
    This test is to verify that DUT can connect classic mouse and
    BLE mouse at the same time.
    """

args_dict = utils.args_to_dict(args)
chameleon_args = hosts.CrosHost.get_chameleon_arguments(args_dict)
newblue_enable = args_dict['newblue'] == 'enable'\
  if 'newblue' in args_dict else False

def run(machine):
    host = hosts.create_host(machine, chameleon_args=chameleon_args)
    bluetooth_test.enable_newblue(host=host, enable=newblue_enable)
    job.run_test('bluetooth_AdapterMDSanity', host=host,
                  num_iterations=1, test_name=NAME.split('.')[1])

parallel_simple(run, machines)
