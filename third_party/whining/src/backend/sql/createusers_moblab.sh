#!/bin/bash
#
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Setup user passwords: this should be done before running [raw]recreate.sh,
# but this can also be run afterwards to reset the passwords.

PWD=$1
HOST=${2:-localhost}
MYSQL="mysql -u root -h $HOST -p$PWD"

if [ -z "$PWD" ]; then
    echo "Usage: $0 dbpassword [host]"
    exit 2
fi

# Reset tty echo on exit
trap "[ -t 0 ] && stty echo" INT HUP TERM 0
[ -t 0 ] && stty -echo
echo -n "wmreader password: "
read -r WMREADER_PASSWORD
echo
echo -n "wmatrix password: "
read -r WMATRIX_PASSWORD
echo

if [ -z "$WMREADER_PASSWORD" -o -z "$WMATRIX_PASSWORD" ]; then
    echo "Please set both passwords."
    exit 1
fi

echo "Creating users:"
echo "CREATE USER 'wmreader'@'%'" | $MYSQL
echo "CREATE USER 'wmatrix'@'%'" | $MYSQL
echo "CREATE USER 'wmreader'@'localhost'" | $MYSQL
echo "CREATE USER 'wmatrix'@'localhost'" | $MYSQL

echo "Setting passwords:"
echo "SET PASSWORD FOR 'wmreader'@'%' = PASSWORD('$WMREADER_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmatrix'@'%' = PASSWORD('$WMATRIX_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmreader'@'localhost' = PASSWORD('$WMREADER_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmatrix'@'localhost' = PASSWORD('$WMATRIX_PASSWORD')" | $MYSQL
echo "FLUSH PRIVILEGES;" | $MYSQL
