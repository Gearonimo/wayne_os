# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Classes and helpers to abstract organizing data for view tables.

Commonly, data is sparsely inserted into a view table.  However, in order
to render the html table, the data must accommodate the following easily:
-enumerate all possible headers (and a count) along both x and y
-transpose the data and allow rendering x vs y or y vs x
-sort either or both x and y in either ascending or descending order
-allow a set of data to be grouped in header blocks (e.g. allow the headers to
 be grouped into columns of 'failed' and 'passed' according to the related
 data.
"""

import os, sys

from src import whining_errors
from src import wmutils


class CellData(object):
    """Class to simplify abstractions for insertion/retrieval of cell data.

    Previously cell data was stored ad-hoc and template code verbosely
    parsed and formatted based on cell data contents. This class should
    enable simpler template code related to formatting cell data.
    """
    def __init__(self):
        """Store in a dict as a key and accumulate instance count with value.

        This is the base implementation, specializations may add metadata.
        """
        self.data = {}
        self.count = 0

    def add(self, data, **kwargs):
        """Simplify accumulating data in a cell.

        Commonly expected data to be a test status string such as: 'GOOD',
        'FAIL', 'ABORT'.

        Args:
            data: value of cell data.
            kwargs: allow custom access for some derived cell-types.
        """
        self.data.setdefault(data, 0)
        incr = kwargs.get('incr', 1)
        self.data[data] += incr
        self.count += incr

    def peek(self):
        """For simple cells; returns the first key in the dict."""
        return self.data.keys()[0] if self.data else None


class GoodFailOtherCellData(CellData):
    """Specialize CellData to support simple status for pass/fail checks."""
    def __init__(self):
        super(GoodFailOtherCellData, self).__init__()
        self._good_names = set()
        self._fail_names = set()
        self._other_names = set()
        self._missing_tests = set()
        self._missing_suites = set()
        # Save the last one only
        self._last_job_tag = None
        self.test_ids = []
        self._fw_rw_version = None
        self._fw_ro_version = None
        self._test_version = None

    @property
    def nattempted(self):
        return self.count

    @property
    def ngood(self):
        return self.data.get('GOOD', 0)

    @property
    def nfail(self):
        return self.data.get('FAIL', 0)

    @property
    def nmissing_tests(self):
        return self.data.get('MISSING_TEST', 0)

    @property
    def nmissing_suites(self):
        return self.data.get('MISSING_SUITE', 0)

    @property
    def nother(self):
        return len(self._other_names)

    def add(self, data, incr=1, test_name=None, suite=None, job_tag=None,
            test_idx=None, fw_rw_version=None, fw_ro_version=None,
            test_version=None):
        super(GoodFailOtherCellData, self).add(data, incr=incr)
        if test_name or suite:
            if data == 'GOOD':
                self._good_names.add(test_name)
            elif data == 'FAIL':
                self._fail_names.add(test_name)
            elif data == 'MISSING_TEST':
                self._missing_tests.add(test_name)
            elif data == 'MISSING_SUITE':
                self._missing_suites.add(suite)
            else:
                self._other_names.add(test_name)
        if job_tag:
            self._last_job_tag = job_tag
        if test_idx:
            self.test_ids.append(test_idx)
        self._fw_rw_version = fw_rw_version
        self._fw_ro_version = fw_ro_version
        self._test_version = test_version

    @property
    def good_names(self):
        return sorted(self._good_names)

    @property
    def fail_names(self):
        return sorted(self._fail_names)

    @property
    def other_names(self):
        return sorted(self._other_names)

    @property
    def missing_tests(self):
        return sorted(self._missing_tests)

    @property
    def missing_suites(self):
        return sorted(self._missing_suites)

    @property
    def job_tag(self):
        return self._last_job_tag

    @property
    def fw_rw_version(self):
        return self._fw_rw_version or '?'

    @property
    def fw_ro_version(self):
        return self._fw_ro_version or '?'

    @property
    def test_version(self):
        return self._test_version or '?'


class CellDataSet(object):
    """Version of CellData that aggregates data supplied."""
    def __init__(self):
        self.data = set()
        self.count = 0

    def add(self, data, **kwargs):
        self.data.add(data)
        self.count += kwargs.get('incr', 1)

    def get_sorted(self):
        """The data set is almost universally used as a sorted list."""
        none_found = (None in self.data)
        if none_found:
            self.data.discard(None)
        data = sorted(self.data)
        if none_found:
            data = [None] + data
        return data


class CellDataList(object):
    """Version of CellData that can aggregate dicts."""
    def __init__(self):
        self.data = []
        self.count = 0

    def add(self, data, **kwargs):
        self.data.append(data)
        self.count += kwargs.get('incr', 1)


class CustomHeaderCollector(object):
    """An object that allows selection of one value from many.

    Custom headers need to reduce data to a single value. For example,
    each result row might include a possible field for the header such
    as start_time.  The header collector needs a corresponding mechanism
    to decide how to reduce it's candidates.  For example, if the header
    is 'time' the method might be min().  If the header is a simple
    lookup the method might be first().
    """
    def __init__(self):
        """Establish default container for custom header data."""
        self.data = {}

    def add(self, key, data):
        """Default handling is to accept the first suggested data.

        Args:
            key: key for the data.
            data: value to hold for the custom header.
        """
        if not key in self.data:
            self.data[key] = data


class CustomHeaderCollector_Min(CustomHeaderCollector):
    """Retains the min supplied data for a header."""
    def add(self, key, data):
        if not key in self.data or self.data[key] > data:
            self.data[key] = data


class TableData(object):
    """Class to simplify insertion/retrieval of html table data.

    This class is an abstraction to simplify common operations of inserting
    data into the structure in data retrieval code and extraction of data
    during the template operations.  This is designed to make both model and
    template code simpler and easier to understand/maintain/modify.

    A restriction imposed is that row header strings and column header strings
    must be unique. This is consistent with our use of row header and col
    header strings as members of the key tuple to the cell data.
    """
    def __init__(self, row_label='', col_label='', title='',
                 cell_default=CellData):
        """Setup storage for headers and cells.

        Args:
            row_label: title of the row headers (e.g. Build)
            col_label: title of the col headers (e.g. Test Name)
            title: title of the table (a comment perhaps)
            cell_default: instance of default cells
        """
        # Labels and title used for table debug printing.
        self._row_label = row_label
        self._col_label = col_label

        self.title = title

        self.cell_default = cell_default

        # transpose is only used when retrieving (not inserting) data. When
        # iterating through data, if transpose is True, key tuples will be
        # constructed in an inverted order.
        self.transpose = False

        # Support sorting by axis labels
        self.sort_rows = True
        self.sort_cols = True

        # row_headers and col_headers are printed along top/side of the table
        # and are sorted for reasonable table viewing.
        #
        # e.g. |-----------|-----------|-----------|-----------|-----------|
        #      | col_label |   col_h1  |  col_h2   | col_h3    | col_h4    |
        #      | row_label |           |           |           |           |
        #      |-----------|-----------|-----------|-----------|-----------|
        #      |           |cust_col_h1|cust_col_h2|cust_col_h3|cust_col_h4|
        #      |-----------|-----------|-----------|-----------|-----------|
        #      | row_h1    |           |           |           |           |
        #      | row_h2    |           |           |           |           |
        #      | row_h3    |           |           |           |           |
        #      | row_h4    |           |           |           |           |
        #      |-----------|-----------|-----------|-----------|-----------|
        self._row_headers = []
        self._col_headers = []

        # Custom headers use _row_headers and _col_headers as keys.
        # See the CustomHeaderCollector class for more details.
        self._custom_row_headers = {}
        self._custom_col_headers = {}

        # Default sort is basic sorted()
        # See SortedBuildTableData for an example of customized sorting.
        self.row_sort_fn = lambda l: sorted(l)
        self.col_sort_fn = lambda l: sorted(l)

        # cell data is indexed with a key of tuple (x, y) where x and y are the
        # corresponding header strings.  For example, it is common for the x
        # dimension to be 'build#'.  The y dimension might be 'test name'. Then
        # the key tuple might be ('R25-1234.0.0', 'platform_Shutdown').
        self._cells = {}

    def _make_key(self, row, col):
        """Intercept key creation in order to handle transposition.

        Args:
            row: row key value (actual values of row-header labels)
            col: col-axis key value (actual values of col-header labels)
        """
        if self.transpose:
            return (col, row)
        return (row, col)

    def get_cell(self, row, col):
        """Common helper to return a CellData instance."""
        k = self._make_key(row, col)
        return self._cells.setdefault(k, self.cell_default())

    def add(self, row, col, data, **kwargs):
        """Adds row and col headers then accumulates a count of cell data.

        Cell data is added according to row and column header.
        Later, if transpose is True, data retrieval will be transposed.
        Initially, cell data is expected to be status (e.g. 'GOOD', 'FAIL').

        Args:
            row: label for the cell row
            col: label for the cell column
            data: value to add/accumulate the count
            kwargs: optional custom data.

        Returns:
           The cell that was acted on.
        """
        if row not in self._row_headers:
            self._row_headers.append(row)
        if col not in self._col_headers:
            self._col_headers.append(col)
        cell = self.get_cell(row, col)
        cell.add(data, **kwargs)
        return cell

    def _init_custom_header(self, custom_header_dict, key, collector_instance):
        """Helper to add CustomHeaderCollectors into the custom_header dicts.

        Args:
            custom_header_dict: dict that collects row and col header data.
            key: row or col identifier into the dict.
            collector_instance: collector object with an add() method.
        """
        # Check if class descends from class CustomHeaderCollector.
        if not isinstance(collector_instance, CustomHeaderCollector):
            raise whining_errors.WhiningTypeError(
                'Expected CustomHeaderCollector (derived) class')
        custom_header_dict[key] = collector_instance

    def init_custom_row_header(self, row, collector_instance):
        """Assign custom row header collectors."""
        self._init_custom_header(self._custom_row_headers, row,
                                 collector_instance)

    def init_custom_col_header(self, col, collector_instance):
        """Assign custom col header collectors."""
        self._init_custom_header(self._custom_col_headers, col,
                                 collector_instance)

    def _add_custom_header(self, custom_header_dict, key1, key2, data):
        """Helper to add data into the CustomHeaderCollectors."""
        custom_header_dict[key1].add(key2, data)

    def add_custom_row_header(self, row, col, data):
        """Add data for a custom row header."""
        self._add_custom_header(self._custom_row_headers, row, col, data)

    def add_custom_col_header(self, col, row, data):
        """Add data for a custom col header."""
        self._add_custom_header(self._custom_col_headers, col, row, data)

    @property
    def row_label(self):
        """Retrieve the row label and handle transpose.

        The row label is usually emitted in column 0 of each row.

        Returns:
            An empty ('') string if no row label supplied on construction.
        """
        return self._col_label if self.transpose else self._row_label

    @property
    def col_label(self):
        """Retrieve the col label and handle transpose.

        The col label is usually emitted in row 0 of each column.

        Returns:
            An empty ('') string if no col label supplied on construction.
        """
        return self._row_label if self.transpose else self._col_label

    def _headers(self, row_first=True):
        """Common helper to retrieve list(s) of row/column headers.

        Handles transposing row and col and proper sorting
        (if sort enabled and sort_fn supplied).

        Args:
            row_first: If True, orientation is rows vs cols.

        Returns:
            list of header strings (sorted if specified).
        """
        no_sort_fn = lambda x: list(x)
        if row_first != self.transpose:
            _headers = self._row_headers
            sort_fn = self.row_sort_fn if self.sort_rows else no_sort_fn
        else:
            _headers = self._col_headers
            sort_fn = self.col_sort_fn if self.sort_cols else no_sort_fn
        return sort_fn(_headers)

    @property
    def row_headers(self):
        """Easy accessor to retrieve sorted list of row headers.

        The expected template pattern (in pseudo-code) for rendering a
        table is:

        Assume: td = TableData()

        Optionally: td.transpose = True

        # emit col headers
        for c in td.col_headers:
            print c
        for r in td.row_headers:
            # emit row header
            print r
            for c in td.col_headers:
                # emit cell data
                print td.cell(r, c)
        """
        return self._headers(row_first=True)

    @property
    def col_headers(self):
        """Easy accessor to retrieve sorted list of col headers.

        See comment for row_headers.
        """
        return self._headers(row_first=False)

    def _custom_header_keys(self, row_first=True):
        """Common helper to retrieve list(s) of custom row/col headers.

        Handles transposing row and col and simple sorting if sort enabled.

        Args:
            row_first: If True, orientation is rows vs cols.

        Returns:
            list of custom header strings (sorted if specified).
        """
        no_sort_fn = lambda x: list(x)
        if row_first != self.transpose:
            _headers = self._custom_row_headers.keys()
            sort_fn = sorted if self.sort_rows else no_sort_fn
        else:
            _headers = self._custom_col_headers.keys()
            sort_fn = sorted if self.sort_cols else no_sort_fn
        return sort_fn(_headers)

    @property
    def custom_row_header_keys(self):
        """Easy accessor to retrieve custom row header keys."""
        return self._custom_header_keys(row_first=True)

    @property
    def custom_col_header_keys(self):
        """Easy accessor to retrieve custom col header keys."""
        return self._custom_header_keys(row_first=False)

    def _custom_header(self, key, row_first=True):
        """Common helper to retrieve custom row/col header data.

        Handles transposing row and col.

        Args:
            row_first: If True, orientation is rows vs cols.

        Returns:
            Data from the custom header dict.
        """
        if row_first != self.transpose:
            _headers = self._custom_row_headers
        else:
            _headers = self._custom_col_headers
        return _headers.get(key)

    def get_custom_row_header(self, key):
        """Easy accessor to retrieve custom row header data."""
        return self._custom_header(key, row_first=True)

    def get_custom_col_header(self, key):
        """Easy accessor to retrieve custom row header data."""
        return self._custom_header(key, row_first=False)

    def to_header(self, header):
        """Common method for converting db fields to headers."""
        return header.replace('_', ' ').title()

    def simple_table(self, top_header=None, cell_classes={}, head_classes={},
                     ignore_rows=[], ignore_cols=[]):
        """Emit the simplest table html to avoid bloating templates.

        Simplifies templates significantly.

        Simple table emits the html for a table with a row for each row in
        self.row_headers and a col for each column in self.col_headers.

        The row_header is NOT emitted by default.

        Args:
            top_header: Optionally include a header across the top.
            cell_classes: Optionally include css formatting classes for cells.
                          Looks for css classes as follows:
                          a) If '_default' present, use it unless col-specific
                             class is found.
                          b) If entry matches col_header (field), use it.
                          See below for a detailed example.
            head_classes: Optionally include css formatting classes for headers.
                          If not provided, defaults to cell_classes.
                          [see cell_classes]
                          See below for a detailed example.
            ignore_rows: List of row_headers for rows to skip.
            ignore_cols: List of col_headers for cols to skip. e.g. it may
                         be common to avoid printing the row_key.
        Returns:
            HTML string to be included in a template.

        Example (using bottlepy templates):

        %_top_title = 'Comments - Test Details'
        %_cell_classes = {'_default': 'failure_table centered',
        %                 'comment': 'failure_table lefted',
        %                 'reason': 'failure_table lefted'}
        %_head_classes = {'_default': 'headeritem centered',
        %                 'comment': 'headeritem lefted',
        %                 'reason': 'headeritem lefted'}
        %_test_td = tpl_vars['data']['test_td']
        {{! _test_td.simple_table(_top_title, _cell_classes, _head_classes) }}
        """
        head_rows = []
        cell_class = cell_classes.get('_default', '')
        header_class = head_classes.get('_default', cell_class)
        if top_header:
            row = """
                <tr>
                  <th class="%s"
                      colspan=%s>
                      %s
                  </th>
                </tr>""" % (header_class, len(self.col_headers), top_header)
            head_rows.append(row)
        row = []
        for h in [t for t in self.col_headers if t not in ignore_cols]:
            h_class = head_classes.get(h, header_class)
            row.append("""
                <th class="%s">
                  %s
                </th>""" % (h_class, self.to_header(h)))
        head_rows.append("""
            <tr>
              %s
            </tr>""" % ''.join(row))

        body_rows = []
        for r in [t for t in self.row_headers if t not in ignore_rows]:
            row = []
            for c in [t for t in self.col_headers if t not in ignore_cols]:
                c_class = cell_classes.get(c, cell_class)
                row.append("""
                    <td class="%s">
                      %s
                    </td>""" % (c_class, self.get_cell(r, c).peek()))
            body_rows.append("""
                <tr>
                  %s
                </tr>""" % ''.join(row))

        head = ['<thead>'] + head_rows + ['</thead>']
        body = ['<tbody>'] + body_rows + ['</tbody>']
        html = ['<table style="width: 100%">'] + head + body + ['</table>']
        return '\n'.join(html)


class SortedBuildTableData(TableData):
    """Specialization of TableData expected to use Build as the row key.

    It is common in test reporting to use Build as a row key. This class
    establishes Build as the row key and adds a custom sort method that
    works with the current Build string (Rww-xx.yy.zz).
    """
    def __init__(self, col_label='', title='', cell_default=CellData):
        """Initialize base TableData and then add the Build sort."""
        super(SortedBuildTableData, self).__init__('Build', col_label, title,
                                                   cell_default)
        self.row_sort_fn = lambda l: sorted(l, cmp=wmutils.cmp_builds,
                                            reverse=True)
