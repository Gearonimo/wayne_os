# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

XORG_MULTILIB=yes
inherit xorg-2

DESCRIPTION="X.Org Randr protocol headers"

KEYWORDS="*"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"
